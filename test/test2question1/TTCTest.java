package test2question1;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TTCTest {

	private static final double DELTA = 1e-15;
	private TTC ttcTrip;
	
	@Before
	public void setUp() throws Exception {
		ttcTrip = new TTC();
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	// R1: A one ay trip inside Zone 1
	@Test
	public void TestR1() {
		double expectedPrice = 2.50;
		String[] from = new String[1];
		from[0] = "Leslie";
		
		String[] to = new String[1];
		to[0] = "Don Mills";
		
		double price = ttcTrip.calculateTotal(from, to);
		System.out.println("R1: " + price);
		assertEquals(expectedPrice, price, DELTA);
		
	}
	
	// R2: A one ay trip inside Zone 2
	@Test
	public void TestR2() {
		double expectedPrice = 3.0;
		String[] from = new String[1];
		from[0] = "Sheppard";
		
		String[] to = new String[1];
		to[0] = "Finch";
		
		double price = ttcTrip.calculateTotal(from, to);
		System.out.println("R2: " + price);
		assertEquals(expectedPrice, price, DELTA);
		
	}
		
	// R3: A trip between zones
	@Test
	public void TestR3() {
		double expectedPrice = 3.0;
		String[] from = new String[1];
		from[0] = "Don Mills";
		
		String[] to = new String[1];
		to[0] = "Finch";
		
		double price = ttcTrip.calculateTotal(from, to);
		System.out.println("R3: " + price);
		assertEquals(expectedPrice, price, DELTA);
		
	}
	
	// R4: More Than 1 trip
	@Test
	public void TestR4() {
		double expectedPrice = 5.50;
		String[] from = new String[2];
		from[0] = "Finch";
		from[1] = "Leslie";
		
		String[] to = new String[2];
		to[0] = "Sheppard";
		to[1] = "Don Mills";
		
		double price = ttcTrip.calculateTotal(from, to);
		System.out.println("R4: " + price);
		assertEquals(expectedPrice, price, DELTA);
		
	}

	// R5: Reaching Daily Maximum
	@Test
	public void TestR5() {
		double expectedPrice = 6.0;
		String[] from = new String[3];
		from[0] = "Finch";
		from[1] = "Sheppard";
		from[2] = "Finch";
		
		String[] to = new String[3];
		to[0] = "Sheppard";
		to[1] = "Finch";
		to[2] = "Sheppard";
		
		double price = ttcTrip.calculateTotal(from, to);
		System.out.println("R5: " + price);
		assertEquals(expectedPrice, price, DELTA);
		
	}
}
